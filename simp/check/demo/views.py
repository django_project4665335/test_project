from django.shortcuts import render,redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.core.mail import send_mail
import random

# Create your views here.

def home(request):
    return render(request, 'demo/home.html')

def generate_otp():
    return str(random.randint(1000,9999))

def register(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']
        user = User.objects.filter(username=username, password=password, email=email)
        otp = generate_otp()
        request.session['otp'] = otp
        send_mail(
            'Your OTP for Registration',
            f'Your OTP is: {otp}',
            'noreply@example.com',
            [email],
            fail_silently = False,
            
        )
        return redirect('login_with_otp')
    return render(request, 'demo/register.html')

def login_with_otp(request):
    if request.method == 'POST':
        email = request.POST['email']
        otp = request.POST['otp']
        stored_otp = request.session.get('otp')
        
        if stored_otp and stored_otp == otp:
            user = User.objects.get(email=email)
            login(request, user)
            del request.session['otp']
            return redirect('home')
        else:
            messages.error(request, 'Invalid OTP.')
    return render(request, 'demo/login_with_otp.html')

@login_required
def logout_view(request):
    logout(request)
    return redirect('home')
    